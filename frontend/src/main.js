// import 'core-js/es6/promise'
// import 'core-js/es6/string'
// import 'core-js/es7/array'
// // import cssVars from 'css-vars-ponyfill'
// global.jQuery = require('jquery');
// var $ = global.jQuery;
// window.$ = $;


/* eslint-disable */
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import api from '@/shared/api'
import VueCookies from 'vue-cookies'
import BootstrapVue from 'bootstrap-vue'

// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css

Vue.use(BootstrapVue)
//Vue.config.productionTip = false
Vue.use(VueCookies)


VueCookies.config("30d")


export const app = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
