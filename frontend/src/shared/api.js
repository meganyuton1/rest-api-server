/* eslint-disable */
let axios = require('axios');
import router from './../router'
import VueCookies from 'vue-cookies'

let axiosInstance = axios.create({
  baseURL: process.env.NODE_ENV == 'production' && process.env.VUE_APP_API_URL ? process.env.VUE_APP_API_URL : process.env.VUE_APP_API_URL_DEV,
  headers: {
    'Authorization': 'Bearer ' + getCookie('token')
  },
  transformRequest: [function (data, headers) {
    // Do whatever you want to transform the data

    headers['Authorization'] = getToken()

    if ( headers['Content-Type'] && headers['Content-Type'].includes('multipart/form-data') ) {
      return data
    }
    else {
      headers['Content-Type'] = "application/json"
      return JSON.stringify(data)
    }

  }],
  validateStatus: function (status) {
    if( status === 401 && !document.location.pathname.includes('/login') ){
      VueCookies.remove('token')
      router.push('/login');
    }
    // if( status === 404 ) {
    //   router.push('/404');
    // }
    else {
      return true;
    }
  }
  /* other custom settings */
});



function getCookie(name) {
  let value = "; " + document.cookie;
  let parts = value.split("; " + name + "=");
  if (parts.length === 2) return parts.pop().split(";").shift();
}

function getToken(){
  return 'Bearer ' + getCookie('token');
}

//module.exports = axiosInstance;
export default axiosInstance;
