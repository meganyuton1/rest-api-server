/* eslint-disable */
import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

// Views
const Main = () => import('@/views/pages/Main')
const Login = () => import('@/views/pages/Login')
const Registration = () => import('@/views/pages/Registration')
const Users = () => import('@/views/pages/Users')

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "/",
            name: "main",
            component: Main
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/registration',
            name: 'Registration',
            component: Registration
        },
        {
            path: '/users',
            name: 'Users',
            component: Users
        },
    ]
});
